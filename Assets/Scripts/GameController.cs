﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public bool stopGame;
    public GameObject shuttle;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private GameObject scoreText, bestScore, canvas;
    [SerializeField] private GameObject[] prefabsStone;
    private int record;
    private float sec, prevSec = 0;
    private int stopEarthquake, maxStoneCount = 1, adCount;
    private bool start;
    private void Start()
    {
        bestScore.GetComponent<Text>().text = PlayerPrefs.GetInt("Record").ToString();
    }
    private void FixedUpdate()
    {
        if (!stopGame)
        {
            //счёт
            sec += Time.deltaTime;
            if (shuttle.GetComponent<MoveController>().enabled)
            {
                record = Mathf.RoundToInt(sec * 5f);
                scoreText.GetComponent<Text>().text = record.ToString();
            }
            //генерация камнепадов
            if (prevSec != sec)
            {
                GameObject[] stones = GameObject.FindGameObjectsWithTag("enemy");
                if (sec > 7 && sec < 70)
                    maxStoneCount = Random.Range(1, 3);
                if (sec > 2)
                    if (stones.Length < maxStoneCount)
                    {
                        GameObject stone = Instantiate(prefabsStone[Random.Range(0, 3)]);
                        stone.transform.position = new Vector3(Random.Range(-1.76f, 1.79f), Random.Range(7f, 9f), -1f);
                        stone.transform.localScale = new Vector3(Random.Range(0.7f, 1f), Random.Range(0.7f, 1f), 1f);
                        stone.transform.eulerAngles = Vector3.forward * Random.Range(0f, 180f);
                    }
                prevSec = sec;
            }
            if (!start)
                StartCoroutine(Earthquake_WaitForSeconds(Random.Range(3f, 20f)));
            if (Mathf.RoundToInt(sec) == stopEarthquake + Random.Range(2, 6))
                Camera.main.GetComponent<Animator>().SetBool("Play", false);
        }
    }

    private IEnumerator Earthquake_WaitForSeconds(float value) //Землетрясение
    {
        start = true;
        yield return new WaitForSeconds(value);
        Camera.main.GetComponent<Animator>().SetBool("Play", true);
        if (GetComponent<AudioSource>().isActiveAndEnabled)
        {
            GetComponent<AudioSource>().clip = audioClip;
            GetComponent<AudioSource>().Play();
        }
        stopEarthquake = Mathf.RoundToInt(sec);
        for (int i = 0; i < Random.Range(1, 4); i++)//Камнепад
        {
            GameObject stone = Instantiate(prefabsStone[Random.Range(0, 3)]);
            int j = Random.Range(0, 2);
            if (j == 0)
            {
                stone.transform.position = new Vector3(Random.Range(-1.76f, 1.39f), Random.Range(7f, 9f), -1f);
                stone.transform.localScale = new Vector3(Random.Range(0.7f, 0.9f), Random.Range(0.7f, 0.9f), 1f);
                stone.transform.eulerAngles = Vector3.forward * Random.Range(0f, 180f);
            }    
            else
            {
                stone.transform.position = new Vector3(Random.Range(-1.36f, 1.79f), Random.Range(7f, 9f), -1f);
                stone.transform.localScale = new Vector3(Random.Range(0.7f, 0.9f), Random.Range(0.7f, 0.9f), 1f);
                stone.transform.eulerAngles = Vector3.forward * Random.Range(0f, 180f);
            }
        }
        start = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)//Всё останавливаем
    {
        if (collision.gameObject.tag == "Player")
        {
            stopGame = true;
            if (PlayerPrefs.GetInt("Record") < record)
                PlayerPrefs.SetInt("Record", record);
            if (PlayerPrefs.GetString("Music") == "on")
                canvas.GetComponent<AudioSource>().enabled = false;
            GetComponent<PlayServicesController>().SendScore(record);
            adCount = PlayerPrefs.GetInt("AdCount");
            if (sec > 6)
                adCount++;
            GameObject.Find("RoadGenerator").GetComponent<RoadGenerator>().enabled = false;
            Time.timeScale = 1f;
            Camera.main.GetComponent<Animator>().SetBool("Play", false);
            StopAllCoroutines();
            record = 0;
            sec = 0;
            prevSec = -1;
            start = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("piece"))
            Destroy(collision.gameObject);
        if (collision.gameObject.tag == "enemy")
            Destroy(collision.gameObject);
        if (collision.gameObject.tag == "platform")
            collision.gameObject.SetActive(false);
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            canvas.transform.GetChild(3).gameObject.SetActive(true);
            GetComponent<BoxCollider2D>().enabled = false;
            GameObject.Find("RoadGenerator").GetComponent<RoadGenerator>().restart();
            foreach (GameObject stone in GameObject.FindGameObjectsWithTag("enemy"))
                Destroy(stone);
            if (adCount > 4)//Показ рекламы после n-го проигрыша
            {
                canvas.GetComponent<AdController>().ShowAd();
                adCount = 0;
            }
            PlayerPrefs.SetInt("AdCount", adCount);
        }
    }
}